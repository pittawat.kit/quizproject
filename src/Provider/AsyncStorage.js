import { AsyncStorage } from '@react-native-async-storage/async-storage'

export const _storeData = async (key, value) => {
    try {
        await AsyncStorage.setItem(key, value);
        return "Y";
    } catch (error) {
        return error;
    }
}

export const _storeObjectData = async (key, value) => {
    try {
        const jsonValue = JSON.stringify(value)
        await AsyncStorage.setItem(key, jsonValue)
        return "Y";
    } catch (error) {
        return error;
    }
}

export const _retrieveData  = async (key) => {
    try {
        const value = await AsyncStorage.getItem(key);
    } catch (error) {
        return error;
    }
}

export const _retrieveObjectData  = async (key) => {
    try {
        const jsonValue = await AsyncStorage.getItem(key)
        return jsonValue != null ? JSON.parse(jsonValue) : null;
    } catch (error) {
        return error;
    }
}
