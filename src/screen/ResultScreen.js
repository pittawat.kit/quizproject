import React, { useState, useEffect } from 'react'
import { View, Text, TouchableOpacity, ImageBackground, StyleSheet, Dimensions,Image } from 'react-native'
const { width, height } = Dimensions.get("window");
export default function ResultScreen({ route, navigation }) {
    const [isLoading, setisLoading] = useState(false);
    const [data, setdata] = useState(route.params.score);

    useEffect(() => {
        setisLoading(true);
        console.log(`[ ResultScreen page ] data ----- >`, data)
        setisLoading(false);
    }, [])

    return (
        <View>
            {
                isLoading == true ? null :
                    <View style={styles.container}>
                        <ImageBackground source={require('../img/15.jpg')} style={styles.image}>
                        <View style={{justifyContent: 'center',height:height}}>
                        <Text  style={[styles.text,{marginTop:-90,textAlign: "center",fontSize: 25,}]}>ResultScreen</Text>
                        <View>
                            <Text style={[styles.text,{}]}>วันที่ทำข้อสอบ : {data.DateTime}</Text>
                            <Text style={[styles.text,{}]}>คะแนนที่ได้ : {data.score}</Text>
                        </View>
                        <TouchableOpacity  style={[styles.background,{height: height * 0.1,width: width * 0.5,alignSelf:'center',
                backgroundColor:'#313D94',alignItems: 'center',marginTop:60
                }]}
                 onPress={() => navigation.reset({
                            index: 0,
                            routes: [{ name: 'MainMenuScreen' }],
                        })}>
                            <View>
                                <Text style={[styles.text,{color:'white',}]}>กลับสู่หน้าหลัก</Text>
                            </View>
                        </TouchableOpacity>
                        </View>
                        </ImageBackground>
                    </View>
            }
        </View>
    )
}
const styles = StyleSheet.create({
    container: {
      // justifyContent: 'center',
      // alignItems: 'center',
      width: '100%',
      height: '100%',
      
    },
    item: {
     backgroundColor:'rgba(100, 100, 100, 0.8)',
      padding: 30,
      marginVertical: 5,
      marginHorizontal: 16
    },
    image: {
      flex: 1,
      // justifyContent: "center"
    },
    text: {
      color: "black",
      fontSize: 18,
      lineHeight: 72,
      fontWeight: "bold",
    //   textAlign: "center",
      // backgroundColor: "#CCFFCCbb",
    },
    background: {
        width: width,
        height: height * 0.25,
        backgroundColor: "#788995",
        justifyContent: "center",
      },
      imgbackground: {
        width: width,
        height: height * 0.25,
        position: "absolute",
        zIndex: 99,
      },
      textlogin: {
        color: "black",
        fontSize: 18,
        alignSelf: "center",
        borderColor: "#707070",
        borderWidth:0.3,
        width:width*0.6,
        paddingStart:30
      },
  });
