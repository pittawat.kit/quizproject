import React, { useState, useEffect } from 'react'
import { View, Text, Alert, StyleSheet, TouchableOpacity, StatusBar, FlatList, ScrollView , ImageBackground, Dimensions} from 'react-native'
import AsyncStorage from '@react-native-async-storage/async-storage'
const { width, height } = Dimensions.get("window");
export default function HistoryScreen({ navigation }) {

    const [isLoading, setisLoading] = useState(false);
    const [historyData, sethistoryData] = useState([]);

    useEffect(() => {
        setisLoading(true);
        getHistory();
        setisLoading(false);
    }, [])

    const getHistory = async () => {
        try {
            var data = await AsyncStorage.getItem("History");
            if (data != null) {
                sethistoryData(JSON.parse(data));   
            }
        } catch (error) {
            Alert.alert("Error getHistory!", error.toString());
        }
    }

    const Item = ({ items }) => (
        <View style={styles.item}>
            <Text style={styles.title}>วันเวลาทำข้อสอบ : {items.DateTime}</Text>
            <Text style={styles.title}>คะแนนรวม: {items.score} / 10</Text>
        </View>
    );

    const renderItem = ({ item }) => (
        <Item items={item} />
    );

    return (
        <View>
            {
                isLoading == true ? null :
                    <View style={styles.container}>
                         <ImageBackground source={require('../img/15.jpg')} style={styles.image}>
                        <Text style={[styles.text,{alignSelf:'center'}]}>HistoryScreen</Text>
                        {
                            historyData.length == 0 ? <View></View> :
                                <View  style={{height:'80%'}}>
                                    <FlatList
                                        data={historyData}
                                        renderItem={renderItem}
                                    />
                                </View>
                        }
                        <TouchableOpacity style={[styles.background,{height: height * 0.1,width: width * 0.5,alignSelf:'center',
                backgroundColor:'#313D94',alignItems: 'center'
                }]}
                  onPress={() => navigation.goBack()}>
                            <View>
                                <Text style={[styles.text,{color:'white'}]}>กลับสู่หน้าหลัก</Text>
                            </View>
                        </TouchableOpacity>
                        </ImageBackground>
                    </View>
            }
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        width: '100%', 
        height: '100%', 
        justifyContent: 'center', 
        alignItems: 'center'
    },
    item: {
        backgroundColor: '#A6EEFC',
        padding: 20,
        marginVertical: 8,
        marginHorizontal: 16,
        
    },
    title: {
        fontSize: 14,
    },
    container: {
        // justifyContent: 'center',
        // alignItems: 'center',
        width: '100%',
        height: '100%',
        
      },
      image: {
        flex: 1,
        // justifyContent: "center"
      },
      text: {
        color: "black",
        fontSize: 18,
        lineHeight: 72,
        fontWeight: "bold",
      //   textAlign: "center",
        // backgroundColor: "#CCFFCCbb",
      },
      background: {
          width: width,
          height: height * 0.25,
          backgroundColor: "#788995",
          justifyContent: "center",
        },
        imgbackground: {
          width: width,
          height: height * 0.25,
          position: "absolute",
          zIndex: 99,
        },
        textlogin: {
          color: "black",
          fontSize: 18,
          alignSelf: "center",
          borderColor: "#707070",
          borderWidth:0.3,
          width:width*0.6,
          paddingStart:30
        },
});
