import React, {useState, useEffect} from 'react'
import { View, Text, Alert, TextInput, TouchableOpacity, ImageBackground, StyleSheet, Dimensions,Image } from 'react-native'
import AsyncStorage from '@react-native-async-storage/async-storage'

const { width, height } = Dimensions.get("window");
export default function RegisterScreen({ navigation }) {

    const [isLoading,setisLoading] = useState(false);
    const [id,setid] = useState('');
    const [name,setname] = useState('');
    const [password,setpassword] = useState('');

    useEffect(() => {
        try {
            setisLoading(true);
            setisLoading(false);
        } catch (error) {
          Alert.alert("Error!",error.toString());
        }
      }, []);

      const setDataStore = async () => {
        try {
            //รหัสนักเรียน
            await AsyncStorage.setItem('name', name);
            await AsyncStorage.setItem('id', id);
            await AsyncStorage.setItem('password', password);
            //ถ้า login มาแล้วเช็ต FirstLogin เป็น false แล้วไม่ต้องมา register ใหม่
            await AsyncStorage.setItem('FirstLogin', "false");
            navigation.reset({
                index: 0,
                routes: [{ name: 'LoginScreen' }],
              });
              
        } catch (error) {
            Alert.alert("Cannot save data to AsyncStorage",error.toString());
        }
    }

    const inputNameChange = (val) => {
        try {
            setname(val)
        } catch (error) {
            Alert.alert("Error", error)
        }
    }

    const inputPasswordChange = (val) => {
        try {
            setpassword(val)
        } catch (error) {
            Alert.alert("Error", error)
        }
    }
    const inputIdChange = (val) => {
        try {
            setid(val)
        } catch (error) {
            Alert.alert("Error", error)
        }
    }

    return (
        <View>
            {
            isLoading == true ? null :
            <View>
                <View style={styles.background}>
                <Image
            source={require("../img/03.jpg")}
            style={styles.imgbackground}
          />
          </View>
                <Text style={styles.text} >RegisterScreen</Text>
                <TextInput
                style={styles.textlogin}
                  value={id}
                  onChangeText={(val) => inputIdChange(val)}
                  placeholder={'id'}/>
                <TextInput
                style={[styles.textlogin,{marginTop:20}]}
                  value={name}
                  onChangeText={(val) => inputNameChange(val)}
                  placeholder={'name'}/>
                  <TextInput
                  style={[styles.textlogin,{marginTop:20}]}
                  value={password}
                  onChangeText={(val) => inputPasswordChange(val)}
                  placeholder={'password'}/>
                <TouchableOpacity onPress={() => setDataStore()} 
                style={[styles.background,{height: height * 0.1,width: width * 0.5,alignSelf:'center',
                backgroundColor:'#132396',marginTop:90
                }]}>
                    <View>
                        <Text style={[styles.text,{color:'white'}]}>สมัคร</Text>
                    </View>
                </TouchableOpacity>
            </View>
            }
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
      // justifyContent: 'center',
      // alignItems: 'center',
      width: '100%',
      height: '100%',
      
    },
    item: {
     backgroundColor:'rgba(100, 100, 100, 0.8)',
      padding: 30,
      marginVertical: 5,
      marginHorizontal: 16
    },
    image: {
      flex: 1,
      // justifyContent: "center"
    },
    text: {
        color: "black",
        fontSize: 18,
        lineHeight: 72,
        fontWeight: "bold",
        textAlign: "center",
        // backgroundColor: "#CCFFCCbb",
      },
    background: {
        width: width,
        height: height * 0.25,
        backgroundColor: "#788995",
        justifyContent: "center",
      },
      imgbackground: {
        width: width,
        height: height * 0.25,
        position: "absolute",
        zIndex: 99,
      },
      textlogin: {
        color: "black",
        fontSize: 18,
        alignSelf: "center",
        borderColor: "#707070",
        borderWidth:0.3,
        width:width*0.6,
        paddingStart:30
      },
  });
