import React, {useState, useEffect} from 'react'
import { View, Text, Alert, TouchableOpacity, ImageBackground, StyleSheet, Dimensions,Image } from 'react-native'
import AsyncStorage from '@react-native-async-storage/async-storage'
const { width, height } = Dimensions.get("window");
export default function MainMenuScreen({ navigation }) {
    const [isLoading,setisLoading] = useState(false);
    const [name,setname] = useState('');
    const [id,setid] = useState('');

    useEffect(() => {
        setisLoading(true);
        getDataFromStore();
        setisLoading(false);
    }, []);

    const getDataFromStore = async () =>{
        try {
            const names = await AsyncStorage.getItem('name');
            const ids = await AsyncStorage.getItem('id');

            setname(names);
            setid(ids);
            console.log(names);
            console.log(ids);
        } catch (error) {
            Alert.alert("Error! getDataFromStore",error.toString());
        }
    }


    const PushToQuizScreen = (val) =>{
        try {
            navigation.navigate('QuizScreen',{ type : val});
        } catch (error) {
            Alert.alert("Error! PushToFinalTestScreen",error.toString());
        }
    }

    const PushToHistoryScreen = () =>{
        try {
            navigation.navigate('HistoryScreen');
        } catch (error) {
            Alert.alert("Error! PushToHistoryScreen",error.toString());
        }
    }

    return (
        <View>
            {
                isLoading == true ? null :
                <View style={styles.container}>
                    <ImageBackground source={require('../img/15.jpg')} style={styles.image}>
                    <Text style={[styles.text,{textAlign: "center"}]}>MainMenuScreen</Text>
                   
                    <Text style={[styles.text]}>ยินดีต้อนรับ : {name}</Text>
                    <Text style={styles.text}>รหัสประจำตัว : {id}</Text>


                    <TouchableOpacity onPress={() => PushToQuizScreen('midterm')}>
                        <View>
                            <Text style={styles.text}>ทำข้อสอบกลางภาค</Text>
                        </View>
                    </TouchableOpacity>

                    <TouchableOpacity onPress={() => PushToQuizScreen('final')}>
                        <View>
                            <Text style={styles.text}>ทำข้อสอบปลายภาค</Text>
                        </View>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={() => PushToHistoryScreen()}>
                        <View>
                            <Text style={styles.text}>ประวัติการทำข้อสอบ</Text>
                        </View>
                    </TouchableOpacity>
                    </ImageBackground>
                </View>
            }
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
      // justifyContent: 'center',
      // alignItems: 'center',
      width: '100%',
      height: '100%',
      
    },
    item: {
     backgroundColor:'rgba(100, 100, 100, 0.8)',
      padding: 30,
      marginVertical: 5,
      marginHorizontal: 16
    },
    image: {
      flex: 1,
      // justifyContent: "center"
    },
    text: {
      color: "black",
      fontSize: 18,
      lineHeight: 72,
      fontWeight: "bold",
    //   textAlign: "center",
      // backgroundColor: "#CCFFCCbb",
    },
    background: {
        width: width,
        height: height * 0.25,
        backgroundColor: "#788995",
        justifyContent: "center",
      },
      imgbackground: {
        width: width,
        height: height * 0.25,
        position: "absolute",
        zIndex: 99,
      },
      textlogin: {
        color: "black",
        fontSize: 18,
        alignSelf: "center",
        borderColor: "#707070",
        borderWidth:0.3,
        width:width*0.6,
        paddingStart:30
      },
  });