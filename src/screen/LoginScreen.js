import React, {useState, useEffect} from 'react'
import { View, Text, Alert, TextInput, TouchableOpacity, ImageBackground, StyleSheet, Dimensions,Image} from 'react-native'
import AsyncStorage from '@react-native-async-storage/async-storage'

const { width, height } = Dimensions.get("window");
export default function LoginScreen({ navigation }) {

    const [isLoading,setisLoading] = useState(false);
    const [password,setpassword] = useState('');
    const [InputPass,setInputPass] = useState('');

    useEffect(() => {
      setisLoading(true);
      getDatafromStore();
      setisLoading(false);
    }, []);

    const getDatafromStore = async () =>{
      try {
        //Document Async Storage
        //https://react-native-async-storage.github.io/async-storage/docs/usage
        const value = await AsyncStorage.getItem('FirstLogin');
        console.log("firstLogin = "+ value);
        if(value == null) {
          navigation.navigate('RegisterScreen');
        } else {
          const pass = await AsyncStorage.getItem('password')
          setpassword(pass);
        }
      } catch (error) {
        Alert.alert("Error! getDataFromStore",error.toString());
      }
    }

      const submit = () =>{
        try {
          //check password from input == password from AsyncStorage
          if (InputPass === password) {
            navigation.navigate('MainMenuScreen');
          } else {
            setInputPass('');
            Alert.alert("Error","รหัสผ่านไม่ถูกต้อง กรุณาลองใหม่");
          }
        } catch (error) {
          Alert.alert("Error Submit!",error.toString());
        }
      }

      const inputTextChange = (val) => {
        try {
            setInputPass(val)
        } catch (error) {
            Alert.alert("Error", error)
        }
    }

    const clearStore = async () =>{
      try {
        const keys = ['FirstLogin', 'name', 'id', 'password']
        await AsyncStorage.multiRemove(keys);
        navigation.navigate('RegisterScreen');
      } catch (error) {
        Alert.alert("Error clearStore", error)
      }
    }

    return (
      <View>
          {
          isLoading == true ? null :
          <View style={styles.container}>
            <ImageBackground source={require('../img/15.jpg')} style={styles.image}>
              <View style={{justifyContent: 'center',height:height}}>
              <Text style={[styles.text,{marginTop:50}]}>LoginScreen</Text>
              
              <TextInput
                style={[styles.textlogin,{marginTop:50}]}
                value={InputPass}
                onChangeText={(val) => inputTextChange(val)}
                placeholder={'Password'}/>
                <TouchableOpacity 
                onPress={() => submit()}
                style={[styles.background,{height: height * 0.1,width: width * 0.5,alignSelf:'center',
                backgroundColor:'#0CC235',marginTop:50
                }]}
                >
                    <View>
                        <Text style={[styles.text,{color:'white'}]}>ล็อกอิน</Text>
                    </View>
                </TouchableOpacity>

                <TouchableOpacity onPress={() => clearStore()} >
                    <View>
                        <Text>Clear Store</Text>
                    </View>
                </TouchableOpacity>
                </View>
                </ImageBackground>
          </View>
          }
      </View>
  )
    
}


const styles = StyleSheet.create({
  container: {
    // justifyContent: 'center',
    // alignItems: 'center',
    width: '100%',
    height: '100%',
    
  },
  item: {
   backgroundColor:'rgba(100, 100, 100, 0.8)',
    padding: 30,
    marginVertical: 5,
    marginHorizontal: 16
  },
  image: {
    flex: 1,
    // justifyContent: "center"
  },
  text: {
    color: "black",
    fontSize: 18,
    lineHeight: 72,
    fontWeight: "bold",
    textAlign: "center",
    // backgroundColor: "#CCFFCCbb",
  },
  background: {
      width: width,
      height: height * 0.25,
      backgroundColor: "#788995",
      justifyContent: "center",
    },
    imgbackground: {
      width: width,
      height: height * 0.25,
      position: "absolute",
      zIndex: 99,
    },
    textlogin: {
      color: "black",
      fontSize: 18,
      alignSelf: "center",
      borderColor: "#707070",
      borderWidth:0.3,
      width:width*0.6,
      paddingStart:30
    },
});
