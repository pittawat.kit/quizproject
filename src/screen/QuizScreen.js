import React, { useState, useEffect } from 'react'
import { View, Text, Alert, StyleSheet, FlatList, StatusBar, TouchableOpacity, ScrollView , ImageBackground} from 'react-native'
import mid1 from '../asset/midterm-test1.json'
import mid2 from '../asset/midterm-test2.json'
import mid3 from '../asset/midterm-test3.json'
import fin1 from '../asset/final-test1.json'
import fin2 from '../asset/final-test2.json'
import fin3 from '../asset/final-test3.json'
import { RadioButton } from 'react-native-paper';

import * as Store from '../Provider/AsyncStorage'
import AsyncStorage from '@react-native-async-storage/async-storage'

export default function QuizScreen({ route, navigation }) {

    const [isLoading, setisLoading] = useState(false);
    const [exam, setexam] = useState([]);
    const [score, setscore] = useState(0);
    const [type, settype] = useState(JSON.stringify(route.params.type));
    const [value0, setValue0] = useState('');
    const [value1, setValue1] = useState('');
    const [value2, setValue2] = useState('');
    const [value3, setValue3] = useState('');
    const [value4, setValue4] = useState('');
    const [value5, setValue5] = useState('');
    const [value6, setValue6] = useState('');
    const [value7, setValue7] = useState('');
    const [value8, setValue8] = useState('');
    const [value9, setValue9] = useState('');
    const [data, setdata] = useState([]);

    useEffect(() => {
        try {
            setisLoading(true);

            var randNum = "";
            do {
                randNum = Math.floor(Math.random() * 4);
            } while (randNum == 0);

            console.log(`randNum`, randNum)

            GetJsonFile(randNum);
            setisLoading(false);
        } catch (error) {
            Alert.alert("Error QuizScreen-useEffect!", error);
        }
    }, []);

    const GetJsonFile = (randNum) => {
        try {
            if (type == '"midterm"') {
                switch (randNum) {
                    case 1:
                        setexam(mid1);
                        break;
                    case 2:
                        setexam(mid2);
                        break;
                    case 3:
                        setexam(mid3);
                        break;
                    case 4:
                        setexam(mid4);
                        break;
                    case 5:
                        setexam(mid5);
                        break;
                    default:
                        setexam(mid1);
                        break;
                }
            } else {
                switch (randNum) {
                    case 1:
                        setexam(fin1);
                        break;
                    case 2:
                        setexam(fin2);
                        break;
                    case 3:
                        setexam(fin3);
                        break;
                    case 4:
                        setexam(fin4);
                        break;
                    case 5:
                        setexam(fin5);
                        break;
                    default:
                        setexam(fin1);
                        break;
                }
            }
        } catch (error) {
            Alert.alert("Error! GetJsonFile", error);
        }
    }

    const submit = () => {
        try {
            Alert.alert('คำเตือน', 'ยืนยันการส่งผลสอบหรือไม่',
                [{ text: 'ใช่', onPress: () => CalculateResult() },
                { text: 'ไม่', onPress: () => console.log('ไม่') }], { cancelable: false },
            );

        } catch (error) {
            Alert.alert("Error!", error);
        }
    }

    const cal = () => {
        var score = 0;
        if (value0 == exam.data[0].ans) {
            score++
        }
        if (value1 == exam.data[1].ans) {
            score++
        }
        if (value2 == exam.data[2].ans) {
            score++
        }
        if (value3 == exam.data[3].ans) {
            score++
        }
        if (value4 == exam.data[4].ans) {
            score++
        }
        if (value5 == exam.data[5].ans) {
            score++
        }
        if (value6 == exam.data[6].ans) {
            score++
        }
        if (value7 == exam.data[7].ans) {
            score++
        }
        if (value8 == exam.data[8].ans) {
            score++
        }
        if (value9 == exam.data[9].ans) {
            score++
        }
        return score;
    }
    const CalculateResult = async () => {
        try {
            var getOldData = await AsyncStorage.getItem("History");
            var today = new Date();
            var date = today.getFullYear() + '-' + (today.getMonth() + 1) + '-' + today.getDate();
            var time = today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();
            var dateTime = date + ' ' + time;

            var sc = cal();

            if (getOldData == null) {
                //ปั้น Data แล้วค่อย set ลง store
                var value = [{
                    "score": sc, //ใส่คะแนนที่คิดได้
                    "DateTime": dateTime
                }];
                console.log(`score ---------> `, sc)
                console.log(`value ---------> `, value)
                var setData = await AsyncStorage.setItem("History", JSON.stringify(value));
                navigation.navigate('ResultScreen' ,({
                    score : value[0]
                }));
            } else {
                getOldData = JSON.parse(getOldData)
                //เอาของเดิม + ของใหม่ค่อยเก็บลง store
                var value2 = {
                    "score": sc, //ใส่คะแนนที่คิดได้
                    "DateTime": dateTime
                };
                getOldData.push(value2);
                console.log(`value2 ---- > `, value2)
                console.log(`getOldData ---- > `, getOldData)
                await AsyncStorage.removeItem("History");
                console.log(`getOldData ---- > `, getOldData )
                await AsyncStorage.setItem("History", JSON.stringify(getOldData));
                var x = await AsyncStorage.getItem("History");
                console.log(`History -------- > `, x)
                navigation.navigate('ResultScreen' ,({
                    score : value2
                }));
            }

        } catch (error) {
            Alert.alert("Error storeDataAndRedirectToResultPage!", error);
        }
    }

    const calchoice = (newValue, no) => {
        if (no == '0') {
            setValue0(newValue)
        } else if (no == '1') {
            setValue1(newValue)
        } else if (no == '2') {
            setValue2(newValue)
        } else if (no == '3') {
            setValue3(newValue)
        } else if (no == '4') {
            setValue4(newValue)
        } else if (no == '5') {
            setValue5(newValue)
        } else if (no == '6') {
            setValue6(newValue)
        } else if (no == '7') {
            setValue7(newValue)
        } else if (no == '8') {
            setValue8(newValue)
        } else if (no == '9') {
            setValue9(newValue)
        }
    }

    return (
        <View>
            {
                isLoading == true ? null :
                    <View style={styles.container}>
                        {type == '"midterm"' ? <Text style={[styles.textHeader,{color:'white'}]}>ข้อสอบกลางภาค</Text> : 
                        <Text style={[styles.textHeader,{color:'white'}]}>ข้อสอบปลายภาค</Text>}
                        {
                            exam.length == 0 ? null :
                                <ScrollView>
                                    <View style={styles.item}>
                                        <Text style={styles.title}>{exam.data[0].question}</Text>
                                        <RadioButton.Group onValueChange={newValue => calchoice(newValue, '0')} value={value0}>
                                            <View>
                                                <Text>ก ) {exam.data[0].choice.a}</Text>
                                                <RadioButton value={exam.data[0].choice.a} />
                                            </View>
                                            <View>
                                                <Text>ข ) {exam.data[0].choice.b}</Text>
                                                <RadioButton value={exam.data[0].choice.b} />
                                            </View>
                                            <View>
                                                <Text>ค ) {exam.data[0].choice.c}</Text>
                                                <RadioButton value={exam.data[0].choice.c} />
                                            </View>
                                            <View>
                                                <Text>ง ) {exam.data[0].choice.d}</Text>
                                                <RadioButton value={exam.data[0].choice.d} />
                                            </View>
                                        </RadioButton.Group>
                                    </View>

                                    <View style={styles.item}>
                                        <Text style={styles.title}>{exam.data[1].question}</Text>
                                        <RadioButton.Group onValueChange={newValue => calchoice(newValue, '1')} value={value1}>
                                            <View>
                                                <Text>ก ) {exam.data[1].choice.a}</Text>
                                                <RadioButton value={exam.data[1].choice.a} />
                                            </View>
                                            <View>
                                                <Text>ข ) {exam.data[1].choice.b}</Text>
                                                <RadioButton value={exam.data[1].choice.b} />
                                            </View>
                                            <View>
                                                <Text>ค ) {exam.data[1].choice.c}</Text>
                                                <RadioButton value={exam.data[1].choice.c} />
                                            </View>
                                            <View>
                                                <Text>ง ) {exam.data[1].choice.d}</Text>
                                                <RadioButton value={exam.data[1].choice.d} />
                                            </View>
                                        </RadioButton.Group>
                                    </View>

                                    <View style={styles.item}>
                                        <Text style={styles.title}>{exam.data[2].question}</Text>
                                        <RadioButton.Group onValueChange={newValue => calchoice(newValue, '2')} value={value2}>
                                            <View>
                                                <Text>ก ) {exam.data[2].choice.a}</Text>
                                                <RadioButton value={exam.data[2].choice.a} />
                                            </View>
                                            <View>
                                                <Text>ข ) {exam.data[2].choice.b}</Text>
                                                <RadioButton value={exam.data[2].choice.b} />
                                            </View>
                                            <View>
                                                <Text>ค ) {exam.data[2].choice.c}</Text>
                                                <RadioButton value={exam.data[2].choice.c} />
                                            </View>
                                            <View>
                                                <Text>ง ) {exam.data[2].choice.d}</Text>
                                                <RadioButton value={exam.data[2].choice.d} />
                                            </View>
                                        </RadioButton.Group>
                                    </View>

                                    <View style={styles.item}>
                                        <Text style={styles.title}>{exam.data[3].question}</Text>
                                        <RadioButton.Group onValueChange={newValue => calchoice(newValue, '3')} value={value3}>
                                            <View>
                                                <Text>ก ) {exam.data[3].choice.a}</Text>
                                                <RadioButton value={exam.data[3].choice.a} />
                                            </View>
                                            <View>
                                                <Text>ข ) {exam.data[3].choice.b}</Text>
                                                <RadioButton value={exam.data[3].choice.b} />
                                            </View>
                                            <View>
                                                <Text>ค ) {exam.data[3].choice.c}</Text>
                                                <RadioButton value={exam.data[3].choice.c} />
                                            </View>
                                            <View>
                                                <Text>ง ) {exam.data[3].choice.d}</Text>
                                                <RadioButton value={exam.data[3].choice.d} />
                                            </View>
                                        </RadioButton.Group>
                                    </View>

                                    <View style={styles.item}>
                                        <Text style={styles.title}>{exam.data[4].question}</Text>
                                        <RadioButton.Group onValueChange={newValue => calchoice(newValue, '4')} value={value4}>
                                            <View>
                                                <Text>ก ) {exam.data[4].choice.a}</Text>
                                                <RadioButton value={exam.data[4].choice.a} />
                                            </View>
                                            <View>
                                                <Text>ข ) {exam.data[4].choice.b}</Text>
                                                <RadioButton value={exam.data[4].choice.b} />
                                            </View>
                                            <View>
                                                <Text>ค ) {exam.data[4].choice.c}</Text>
                                                <RadioButton value={exam.data[4].choice.c} />
                                            </View>
                                            <View>
                                                <Text>ง ) {exam.data[4].choice.d}</Text>
                                                <RadioButton value={exam.data[4].choice.d} />
                                            </View>
                                        </RadioButton.Group>
                                    </View>

                                    <View style={styles.item}>
                                        <Text style={styles.title}>{exam.data[5].question}</Text>
                                        <RadioButton.Group onValueChange={newValue => calchoice(newValue, '5')} value={value5}>
                                            <View>
                                                <Text>ก ) {exam.data[5].choice.a}</Text>
                                                <RadioButton value={exam.data[5].choice.a} />
                                            </View>
                                            <View>
                                                <Text>ข ) {exam.data[5].choice.b}</Text>
                                                <RadioButton value={exam.data[5].choice.b} />
                                            </View>
                                            <View>
                                                <Text>ค ) {exam.data[5].choice.c}</Text>
                                                <RadioButton value={exam.data[5].choice.c} />
                                            </View>
                                            <View>
                                                <Text>ง ) {exam.data[5].choice.d}</Text>
                                                <RadioButton value={exam.data[5].choice.d} />
                                            </View>
                                        </RadioButton.Group>
                                    </View>

                                    <View style={styles.item}>
                                        <Text style={styles.title}>{exam.data[6].question}</Text>
                                        <RadioButton.Group onValueChange={newValue => calchoice(newValue, '6')} value={value6}>
                                            <View>
                                                <Text>ก ) {exam.data[6].choice.a}</Text>
                                                <RadioButton value={exam.data[6].choice.a} />
                                            </View>
                                            <View>
                                                <Text>ข ) {exam.data[6].choice.b}</Text>
                                                <RadioButton value={exam.data[6].choice.b} />
                                            </View>
                                            <View>
                                                <Text>ค ) {exam.data[6].choice.c}</Text>
                                                <RadioButton value={exam.data[6].choice.c} />
                                            </View>
                                            <View>
                                                <Text>ง ) {exam.data[6].choice.d}</Text>
                                                <RadioButton value={exam.data[6].choice.d} />
                                            </View>
                                        </RadioButton.Group>
                                    </View>

                                    <View style={styles.item}>
                                        <Text style={styles.title}>{exam.data[7].question}</Text>
                                        <RadioButton.Group onValueChange={newValue => calchoice(newValue, '7')} value={value7}>
                                            <View>
                                                <Text>ก ) {exam.data[7].choice.a}</Text>
                                                <RadioButton value={exam.data[7].choice.a} />
                                            </View>
                                            <View>
                                                <Text>ข ) {exam.data[7].choice.b}</Text>
                                                <RadioButton value={exam.data[7].choice.b} />
                                            </View>
                                            <View>
                                                <Text>ค ) {exam.data[7].choice.c}</Text>
                                                <RadioButton value={exam.data[7].choice.c} />
                                            </View>
                                            <View>
                                                <Text>ง ) {exam.data[7].choice.d}</Text>
                                                <RadioButton value={exam.data[7].choice.d} />
                                            </View>
                                        </RadioButton.Group>
                                    </View>

                                    <View style={styles.item}>
                                        <Text style={styles.title}>{exam.data[8].question}</Text>
                                        <RadioButton.Group onValueChange={newValue => calchoice(newValue, '8')} value={value8}>
                                            <View>
                                                <Text>ก ) {exam.data[8].choice.a}</Text>
                                                <RadioButton value={exam.data[8].choice.a} />
                                            </View>
                                            <View>
                                                <Text>ข ) {exam.data[8].choice.b}</Text>
                                                <RadioButton value={exam.data[8].choice.b} />
                                            </View>
                                            <View>
                                                <Text>ค ) {exam.data[8].choice.c}</Text>
                                                <RadioButton value={exam.data[8].choice.c} />
                                            </View>
                                            <View>
                                                <Text>ง ) {exam.data[8].choice.d}</Text>
                                                <RadioButton value={exam.data[8].choice.d} />
                                            </View>
                                        </RadioButton.Group>
                                    </View>

                                    <View style={styles.item}>
                                        <Text style={styles.title}>{exam.data[9].question}</Text>
                                        <RadioButton.Group onValueChange={newValue => calchoice(newValue, '9')} value={value9}>
                                            <View>
                                                <Text>ก ) {exam.data[9].choice.a}</Text>
                                                <RadioButton value={exam.data[9].choice.a} />
                                            </View>
                                            <View>
                                                <Text>ข ) {exam.data[9].choice.b}</Text>
                                                <RadioButton value={exam.data[9].choice.b} />
                                            </View>
                                            <View>
                                                <Text>ค ) {exam.data[9].choice.c}</Text>
                                                <RadioButton value={exam.data[9].choice.c} />
                                            </View>
                                            <View>
                                                <Text>ง ) {exam.data[9].choice.d}</Text>
                                                <RadioButton value={exam.data[9].choice.d} />
                                            </View>
                                        </RadioButton.Group>
                                    </View>
                                </ScrollView>
                        }

                        <TouchableOpacity style={styles.submitBtn} onPress={() => submit()}>
                            <View>
                                <Text style={{ fontSize: 18,color:'white' }}>ส่งคำตอบ</Text>
                            </View>
                        </TouchableOpacity>
                    </View>
            }
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        justifyContent: 'center',
        alignItems: 'center',
        width: '100%',
        height: '100%',
        backgroundColor: 'white'
    },
    item: {
        backgroundColor: 'white',
        padding: 20,
        marginVertical: 8,
        marginHorizontal: 16,
    },
    title: {
        fontSize: 14,
    },
    textHeader: {
        fontSize: 20,
        padding: 10,
        width: '100%',
        padding: 10,
        backgroundColor: '#313D94',
        textAlign: 'center'
    },
    submitBtn: {
        alignItems: 'center',
        height: '7%',
        justifyContent: 'center',
        backgroundColor: '#313D94',
        width: '100%'
    }
});
